from django.forms import ChoiceField


def init_choice_field(queryset, required=False):
    choices = queryset
    if not required:
        choices.insert(0, ('', '-----------'))
    return ChoiceField(
        choices=choices,
        required=required,
    )


def list_all_subjects(with_ref=True):
    from curriculum.models import Class
    if with_ref:
        return Class.objects.all().values_list('pk', 'subject')
    return Class.objects.all().values_list('subject', flat=True)


def list_subjects(class_ids):
    from curriculum.models import Class
    return Class.objects.filter(pk__in=class_ids).values_list('subject', flat=True)


def list_course_students(course_id):
    from school.models import Student
    return Student.objects.filter(course=course_id)


def list_classes_students(class_id):
    from school.models import Student
    return Student.objects.filter(classes__contains=class_id)
