from django.contrib import admin

from .models import Class, Course
from .forms import ClassForm, CourseForm


class ClassAdmin(admin.ModelAdmin):
    form = ClassForm
    list_display = ['name', 'number_of_students']


class CourseAdmin(admin.ModelAdmin):
    form = CourseForm
    list_display = ['name', 'number_of_enrolled_students', 'number_of_teachers']


# Register your models here.
admin.site.register(Class, ClassAdmin)
admin.site.register(Course, CourseAdmin)
