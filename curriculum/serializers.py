from rest_framework import serializers

from .models import Subject, Class, Course
from school.models import Student


class ClassSerializer(serializers.ModelSerializer):
    class Meta:
        model = Class
        fields = ('name',  'max_capacity', 'subject', 'number_of_students', 'assigned_teacher')


class CourseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ('name', 'code', 'required_subjects', 'number_of_enrolled_students', 'number_of_teachers')
