from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import ClassViewSet, CourseViewSet

# Creater router and register viewset
router = DefaultRouter()
# router.register(r'subjects', SubjectViewSet)
router.register(r'classes', ClassViewSet)
router.register(r'courses', CourseViewSet)

urlpatterns = [
    path(r'', include(router.urls)),
]