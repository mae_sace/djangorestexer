from djongo import models
from django import forms
from bson.objectid import ObjectId

from utils import init_choice_field, list_all_subjects, list_subjects, list_course_students, list_classes_students

YEAR_LEVEL_CHOICES = (
    (1, 'Year Level 1'),
    (2, 'Year Level 2'),
    (3, 'Year Level 3'),
    (4, 'Year Level 4'),
    (5, 'Year Level 5'),
    (6, 'Year Level 6'),
    (7, 'Year Level 7'),
)


class Course(models.Model):
    """
    Fields: name, code, required_subjects

    Attributes:
    - Has a list of subjects required for it
    """
    _id = models.ObjectIdField(primary_key=True)

    name = models.CharField(max_length=200)
    code = models.CharField(max_length=10, unique=True)

    required_subjects = models.ListField()

    objects = models.DjongoManager()

    def __str__(self):
        return '{}'.format(self.name)

    def list_required_subjects(self):
        return list_subjects(self.required_subjects)

    def number_of_enrolled_students(self):
        """
        Number of students enrolled to the course
        """
        return list_course_students(self.pk).count()

    def number_of_teachers(self):
        """
        Number of teachers who teach subjects under the course
        """
        subj_ref = [subj[0] for subj in list_all_subjects() if subj[1].course == str(self.pk)]
        return Class.objects.filter(pk__in=subj_ref, assigned_teacher__isnull=False).count()


class Subject(models.Model):
    """
    Fields: name, code, year_level

    Attributes:
    - Can optionally belong to a particular course
    """
    _id = models.ObjectIdField(primary_key=True)

    name = models.CharField(max_length=200)
    code = models.CharField(max_length=5, unique=True)
    year_level = models.IntegerField(choices=YEAR_LEVEL_CHOICES, default=1)

    course = models.CharField(max_length=200)

    objects = models.DjongoManager()

    class Meta:
        abstract = True

    def __str__(self):
        return '{}'.format(self.name)


class SubjectForm(forms.ModelForm):
    class Meta:
        model = Subject
        fields = (
            'name',
            'code',
            'year_level',
            'course',
        )

    def __init__(self, *args, **kwargs):
        super(SubjectForm, self).__init__(*args, **kwargs)
        course_choices = [(ObjectId(course.pk), course.name) for course in Course.objects.all()]
        self.fields['course'] = init_choice_field(course_choices)


class Class(models.Model):
    """
    Fields: name, max_capacity, teacher

    Attributes:
    - Corresponds to one subject
    - Has one teacher assigned to it
    - Can have multiple students

    Constraints:
    - If the number of students in a class is already equal to the max capacity of the class, 
      new students cannot be added to it anymore.
    """
    _id = models.ObjectIdField(primary_key=True)

    name = models.CharField(max_length=200)
    max_capacity = models.IntegerField()

    subject = models.EmbeddedModelField(
        model_container=Subject,
        model_form_class=SubjectForm,
    )
    assigned_teacher = models.CharField(max_length=200, null=True, blank=True)

    objects = models.DjongoManager()

    class Meta:
        verbose_name_plural = "classes"

    def __unicode__(self):
        return '{}'.format(self.name)

    def __str__(self):
        return '{}'.format(self.name)

    def number_of_students(self):
        return list_classes_students(self.pk).count()

    def required_year_level(self):
        return self.subject.year_level
