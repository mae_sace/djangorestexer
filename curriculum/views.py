from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import viewsets, status, generics
from rest_framework.decorators import action, detail_route
from rest_framework.response import Response

from .models import Class, Course
from .serializers import ClassSerializer, CourseSerializer
from school.models import Teacher
from school.serializers import StudentSerializer, TeacherSerializer

'''
class SubjectViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Subject.objects.all()
    serializer_class = SubjectSerializer

    @action(detail=True)
    def teachers(self, request, pk=None):
        """
        Get list of teachers teaching a particular subject
        """
        subject = self.get_object()
        teachers = Teacher.objects.filter(assigned_classes__subject__id=subject.id)
        serializer = TeacherSerializer(teachers, many=True)
        return Response(serializer.data)

    @action(detail=True)
    def required_in_courses(self, request, pk=None):
        """
        Get list of courses a particular subject is being required
        """
        subject = self.get_object()
        courses = subject.required_in_courses.all()
        serializer = CourseSerializer(courses,many=True)
        return Response(serializer.data)
'''

class ClassViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Class.objects.all()
    serializer_class = ClassSerializer

    @action(detail=True)
    def students(self, request, pk=None):
        """
        Get list of students for a particular class
        """
        class_obj = self.get_object()
        students = class_obj.student_list.all()
        serializer = StudentSerializer(students,many=True)
        return Response(serializer.data)

class CourseViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Course.objects.all()
    serializer_class = CourseSerializer
