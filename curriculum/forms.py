from django import forms
from bson.objectid import ObjectId

from curriculum.models import Class, Course
from school.models import Teacher
from utils import init_choice_field, list_all_subjects


class ClassForm(forms.ModelForm):
    class Meta:
        model = Class
        fields = (
            'name',
            'max_capacity',
            'subject',
            'assigned_teacher',
        )

    def __init__(self, *args, **kwargs):
        super(ClassForm, self).__init__(*args, **kwargs)
        teacher_choices = [(ObjectId(teacher.pk), teacher.name) for teacher in Teacher.objects.all()]
        self.fields['assigned_teacher'] = init_choice_field(teacher_choices)


class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = (
            'name',
            'code',
            'required_subjects',
        )

    def __init__(self, *args, **kwargs):
        super(CourseForm, self).__init__(*args, **kwargs)
        self.fields['required_subjects'] = forms.MultipleChoiceField(
            choices=[(subject[0], subject[1].name) for subject in list_all_subjects()],
            required=False,
        )
