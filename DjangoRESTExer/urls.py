from django.conf.urls import include
from django.urls import path
from django.contrib import admin
from rest_framework.schemas import get_schema_view

from school.views import api_root

schema_view = get_schema_view(title='Pastebin API')

urlpatterns = [
    path(r'', api_root),
    path(r'', include(('school.urls', 'school'), namespace='school')),
    path(r'', include(('curriculum.urls', 'curriculum'), namespace='curriculum')),
    path(r'admin/', admin.site.urls),
    path(r'schema/', schema_view),
]
