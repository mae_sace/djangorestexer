from django.contrib import admin

from .models import Teacher, Student

from .forms import TeacherForm, StudentForm


class TeacherAdmin(admin.ModelAdmin):
    form = TeacherForm


class StudentAdmin(admin.ModelAdmin):
    form = StudentForm


# Register your models here.
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Student, StudentAdmin)
