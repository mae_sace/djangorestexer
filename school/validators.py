def check_if_list_subset(list1,list2):
    """
    Check if list1 contains all elements of list2
    """
    return all(elem in list1 for elem in list2)