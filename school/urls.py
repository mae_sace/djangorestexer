from django.conf.urls import include
from django.urls import path
from rest_framework.routers import DefaultRouter

from .views import TeacherViewSet, StudentViewSet, ClassStudentView, ClassStudentCreateView, ClassStudentDeleteView

# Creater router and register viewset
router = DefaultRouter()
router.register(r'teachers', TeacherViewSet)
router.register(r'students', StudentViewSet)

urlpatterns = [
    path(r'class/<int:pk>/', ClassStudentView.as_view()),
    path(r'class/<int:class_id>/enroll/', ClassStudentCreateView.as_view()),
    path(r'class/<int:class_id>/unenroll/<int:pk>', ClassStudentDeleteView.as_view()),
    path(r'', include(router.urls)),
]