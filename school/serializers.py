from rest_framework import serializers

from .models import Teacher, Student
from curriculum.models import Class
from .validators import check_if_list_subset


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('first_name', 'last_name', 'year_level', 'course')


class TeacherSerializer(serializers.ModelSerializer):    
    class Meta:
        model = Teacher
        fields = ('first_name','last_name','teachable_classes','assigned_classes')

    '''
    def validate(self, data):
        assigned_classes = data['assigned_classes']
        teachable_classes = data['teachable_classes']
        if not check_if_list_subset(teachable_classes,assigned_classes):
            # A teacher cannot be assigned to a class wherein the subject 
            # isn’t among the list of subjects he/she can teach.
            raise serializers.ValidationError('Cannot assign class not included in classes teacher can teach.')
        return super().validate(data)
    '''


class ClassStudentSerializer(StudentSerializer):
    kwarg_lookup = 'class_id'

    '''
    def validate(self, data):
        class_id = self.context.get(self.kwarg_lookup)
        clz = Class.objects.get(pk=class_id)
        if clz.number_of_students() >= clz.max_capacity:
            # If the number of students in a class is already equal to the max 
            # capacity of the class, new students cannot be added to it anymore.
            raise serializers.ValidationError('Cannot add student to class. Class is already full.')
        if clz.required_year_level() != data['year_level']:
            # A student cannot take a class if his/her year level is not 
            # the same as the required year level for the subject.
            raise serializers.ValidationError('Cannot add student to class. Student year level not same as class subject.')
        return super().validate(data)
    '''