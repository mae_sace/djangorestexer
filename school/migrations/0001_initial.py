# Generated by Django 2.0.5 on 2018-06-01 01:09

from django.db import migrations, models
import django.db.models.deletion
import djongo.models.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('curriculum', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('_id', djongo.models.fields.ObjectIdField(primary_key=True, serialize=False)),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('year_level', models.IntegerField(choices=[(1, 'Year Level 1'), (2, 'Year Level 2'), (3, 'Year Level 3'), (4, 'Year Level 4'), (5, 'Year Level 5'), (6, 'Year Level 6'), (7, 'Year Level 7')], default=1)),
                ('classes', djongo.models.fields.ArrayReferenceField(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='student', to='curriculum.Class')),
            ],
        ),
        migrations.CreateModel(
            name='Teacher',
            fields=[
                ('_id', djongo.models.fields.ObjectIdField(primary_key=True, serialize=False)),
                ('first_name', models.CharField(max_length=200)),
                ('last_name', models.CharField(max_length=200)),
                ('assigned_classes', djongo.models.fields.ArrayReferenceField(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='assigned_teachers', to='curriculum.Class')),
                ('teachable_classes', djongo.models.fields.ArrayReferenceField(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='teacher', to='curriculum.Class')),
            ],
        ),
    ]
