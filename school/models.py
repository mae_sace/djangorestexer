from djongo import models

from curriculum.models import Subject, Class, Course, YEAR_LEVEL_CHOICES


class Teacher(models.Model):
    """
    Fields: first_name, last_name, teachable_classes

    Attributes:
    - Can teach multiple classes
    - Has multiple classes assigned to him/her

    Constraints:
    - Teacher cannot be assigned to a class wherein the subject isn’t among the list of subjects he/she can teach.
    """
    _id = models.ObjectIdField()

    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)

    teachable_classes = models.ListField()

    objects = models.DjongoManager()

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def name(self):
        return '{} {}'.format(self.first_name, self.last_name)


class Student(models.Model):
    """
    Fields: first_name, last_name, year_level

    Attributes:
    - Belongs to one course.
    - Can have multiple classes.
    - Has a list of required subjects for a particular year level

    Constraints:
    - Student cannot take a class if his/her year level is not the same as the required year level for the subject.
    """
    _id = models.ObjectIdField()

    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    year_level = models.IntegerField(choices=YEAR_LEVEL_CHOICES,default=1)

    course = models.CharField(max_length=200)
    classes = models.ListField()

    objects = models.DjongoManager()

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def name(self):
        return '{} {}'.format(self.first_name, self.last_name)

    def required_subjects(self):
        course = Course.objects.filter(pk=self.course)
        return course.list_required_subjects()

    def required_subjects_for_year_level(self):
        subjects = self.required_subjects()
        return [subj for subj in subjects if subj.year_level == self.year_level]
