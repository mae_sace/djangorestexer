from django import forms

from .models import Teacher, Student
from curriculum.models import Class, Course
from utils import init_choice_field


class TeacherForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = (
            'first_name',
            'last_name',
            'teachable_classes',
        )

    def __init__(self, *args, **kwargs):
        super(TeacherForm, self).__init__(*args, **kwargs)
        self.fields['teachable_classes'] = forms.MultipleChoiceField(
            choices=[(cls.pk, cls.name) for cls in Class.objects.all()],
            required=False,
        )


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        fields = (
            'first_name',
            'last_name',
            'year_level',
            'course',
            'classes'
        )

    def __init__(self, *args, **kwargs):
        super(StudentForm, self).__init__(*args, **kwargs)
        courses = [(course.pk, course.name) for course in Course.objects.all()]
        self.fields['course'] = init_choice_field(courses)
        self.fields['classes'] = forms.MultipleChoiceField(
            choices=[(cls.pk, cls.name) for cls in Class.objects.all()],
            required=False,
        )
