from django.shortcuts import render
from django.core.exceptions import ValidationError
from rest_framework import viewsets, generics, mixins
from rest_framework.decorators import api_view, action
from rest_framework.response import Response
from rest_framework.reverse import reverse

from .models import Teacher, Student
from .serializers import TeacherSerializer, StudentSerializer, ClassStudentSerializer
from curriculum.serializers import ClassSerializer
from curriculum.models import Subject, Class

@api_view(['GET'])
def api_root(request, format=None):
    """
    Lists out all API root endpoints
    """
    return Response({
        'teachers': reverse('school:teacher-list', request=request, format=format),
        'students': reverse('school:student-list', request=request, format=format),
        # 'subjects': reverse('curriculum:subject-list', request=request, format=format),
        'classes': reverse('curriculum:class-list', request=request, format=format),
        'courses': reverse('curriculum:course-list', request=request, format=format),
    })

class TeacherViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Teacher.objects.all()
    serializer_class = TeacherSerializer

class StudentViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.
    """
    queryset = Student.objects.all()
    serializer_class = StudentSerializer

    @action(detail=True)
    def classes(self, request, pk=None):
        """
        Get list of classes in which a particular student belongs
        """
        student = self.get_object()
        classes = student.classes.all()
        serializer = ClassSerializer(classes,many=True)
        return Response(serializer.data)

    @action(detail=True)
    def teachers(self, request, pk=None):
        """
        Get list of teachers a student has for his/her classes
        """
        student = self.get_object()
        teacher_ids = student.classes.values_list('assigned_teacher')
        teachers = Teacher.objects.filter(pk=teacher_ids)
        serializer = TeacherSerializer(teachers,many=True)
        return Response(serializer.data)


class ClassStudentView(generics.RetrieveAPIView):
    """
    Display class details
    """
    queryset = Class.objects.all()
    serializer_class = ClassSerializer

class ClassStudentCreateView(generics.GenericAPIView,
                             mixins.CreateModelMixin):
    """
    Add student to a class
    """
    queryset = Student.objects.all()
    serializer_class = ClassStudentSerializer
    kwarg_lookup = 'class_id'

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get_class_id(self):
        return self.kwargs.get(self.kwarg_lookup)

    def get_serializer_context(self):
        context = super(ClassStudentCreateView, self).get_serializer_context()
        context.update({
            "class_id": self.get_class_id()
        })
        return context

    def perform_create(self, serializer):
        class_id = self.get_class_id()
        clz = Class.objects.get(pk=class_id)
        instance = serializer.save()
        instance.classes.add(clz)
        

class ClassStudentDeleteView(generics.DestroyAPIView):
    """
    Remove student from a class
    """
    serializer_class = ClassStudentSerializer
    kwarg_lookup = 'class_id'

    def get_queryset(self,**kwargs):
        class_id = self.kwargs.get(self.kwarg_lookup)
        student = Student.objects.filter(classes__id=class_id)
        return student
